package ru.fizteh.fivt.vitaly_pavlenko;

import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;
import ru.fizteh.fivt.vitaly_pavlenko.interpreter.Command;
import ru.fizteh.fivt.vitaly_pavlenko.interpreter.Interpreter;
import ru.fizteh.fivt.vitaly_pavlenko.interpreter.InterpreterState;

import java.io.IOException;
import java.util.Map;
import java.util.function.BiConsumer;

public class FileMapInterpreter {
    public static void main(String[] args) throws Exception {
        String pathName = System.getProperty("db.file");
        if (pathName == null) {
            System.err.println("You must specify db.file via -Ddb.file JVM parameter");
            System.exit(1);
        }

        try (FileStoredStringMap fileStoredStringMap = new FileStoredStringMap(System.getProperty("db.file"))) {
            StringMapInterpreterState stringMapInterpreterState = new StringMapInterpreterState(fileStoredStringMap);
            new Interpreter(stringMapInterpreterState, new Command[]{
                new Command("put", 2, new BiConsumer<InterpreterState, String[]>() {
                    @Override
                    public void accept(InterpreterState interpreterState, String[] arguments) {
                        StringMapInterpreterState state = (StringMapInterpreterState) interpreterState;
                        Map<String, String> map = state.getMap();
                        String key = arguments[0];
                        String value = arguments[1];

                        if (map.containsKey(key)) {
                            System.out.println("overwrite");
                            System.out.println(map.get(key));
                        } else {
                            System.out.println("new");
                        }
                        map.put(arguments[0], arguments[1]);
                    }
                }),
                new Command("list", 0, new BiConsumer<InterpreterState, String[]>() {
                    @Override
                    public void accept(InterpreterState interpreterState, String[] arguments) {
                        StringMapInterpreterState state = (StringMapInterpreterState) interpreterState;
                        Map<String, String> map = state.getMap();

                        System.out.println(String.join(", ", map.keySet()));
                    }
                }),
                new Command("get", 1, new BiConsumer<InterpreterState, String[]>() {
                    @Override
                    public void accept(InterpreterState interpreterState, String[] arguments) {
                        StringMapInterpreterState state = (StringMapInterpreterState) interpreterState;
                        Map<String, String> map = state.getMap();
                        String key = arguments[0];

                        if (map.containsKey(key)) {
                            System.out.println("found");
                            System.out.println(map.get(key));
                        } else {
                            System.out.println("not found");
                        }
                    }
                }),
                new Command("remove", 1, new BiConsumer<InterpreterState, String[]>() {
                    @Override
                    public void accept(InterpreterState interpreterState, String[] arguments) {
                        StringMapInterpreterState state = (StringMapInterpreterState) interpreterState;
                        Map<String, String> map = state.getMap();
                        String key = arguments[0];

                        if (map.containsKey(key)) {
                            System.out.println("removed");
                            map.remove(key);
                        } else {
                            System.out.println("not found");
                        }
                    }
                })
            }).run(args);
        }
    }
}
