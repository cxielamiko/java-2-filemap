package ru.fizteh.fivt.vitaly_pavlenko;

import ru.fizteh.fivt.vitaly_pavlenko.interpreter.InterpreterState;

import java.io.IOException;
import java.util.Map;

/**
 * Created by vpavlenko on 15.10.14.
 */
public class StringMapInterpreterState implements InterpreterState {
    private final Map<String, String> map;

    public StringMapInterpreterState(Map<String, String> map) throws IOException {
        this.map = map;
    }

    public Map<String, String> getMap() {
        return map;
    }
}
